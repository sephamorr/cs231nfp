args = None
if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description="Trains a convolutional neural network and saves it to file")
    parser.add_argument('modelStoreFile', 
        type=argparse.FileType('r'), nargs='?',
        help="The file storing the saved model params to preload the model with" )
    parser.add_argument('--repl', action='store_true',
        help="Puts the model into repl mode after setting up model.")
    args = parser.parse_args()


import cv2.cv as cv
import cv2

import numpy as np

import io
import scipy.misc

import lasagne
from lasagne.layers import InputLayer, DenseLayer, DropoutLayer
from lasagne.layers.dnn import Conv2DDNNLayer as ConvLayer
# from lasagne.layers import Conv2DLayer as ConvLayer
from lasagne.layers import MaxPool2DLayer as PoolLayer
from lasagne.layers import LocalResponseNormalization2DLayer as NormLayer
from lasagne.utils import floatX

import pickle

import theano.tensor as T
import theano
import theano.compile.nanguardmode
import h5py
import progressbar

import math
import sys
import lz4
import StringIO

NUM_EPOCHS = 12
LEARNING_RATE = 0.006
DATA_DIR = "/media/xiaonan/NeuralNet Data/"

CORR_THR = 0.5
def estCorrectness(output, actual):
    return np.sum(np.sum((output>CORR_THR)==actual,  axis=-1)==6)


def makeNeuralNet(input_var=None):
    net = {}
    net['input'] = InputLayer(shape=(None, 3, 224, 224), input_var=input_var)
    net['conv1'] = ConvLayer(net['input'], num_filters=96, filter_size=7, stride=2)
    net['norm1'] = NormLayer(net['conv1'], alpha=0.0001) # caffe has alpha = alpha * pool_size
    net['pool1'] = PoolLayer(net['norm1'], pool_size=3, stride=3, ignore_border=False)
    net['conv2'] = ConvLayer(net['pool1'], num_filters=256, filter_size=5)
    net['pool2'] = PoolLayer(net['conv2'], pool_size=2, stride=2, ignore_border=False)
    net['conv3'] = ConvLayer(net['pool2'], num_filters=512, filter_size=3, pad=1)
    net['pool5'] = PoolLayer(net['conv3'], pool_size=3, stride=3, ignore_border=False)
    net['fc6'] = DenseLayer(net['pool5'], num_units=1024)
    net['drop6'] = DropoutLayer(net['fc6'], p=0.5)
#     net['fc7'] = DenseLayer(net['drop6'], num_units=4096)
#     net['drop7'] = DropoutLayer(net['fc7'], p=0.5)
#     net['fc8'] = DenseLayer(net['drop7'], num_units=1000, nonlinearity=lasagne.nonlinearities.softmax)
#     output_layer_vgg = net['fc8']
    net['_fc7'] = DenseLayer(net['drop6'], num_units=1024)
    net['_drop7'] = DropoutLayer(net['_fc7'], p=0.5)
    net['_fc8out'] = DenseLayer(net['_drop7'], num_units=6, nonlinearity=lasagne.nonlinearities.sigmoid)
    output_layer_driver = net['_fc8out']
    return  output_layer_driver
    
# def log_softmax(x):
#     xdev = x - x.max(1, keepdims=True)
#     return xdev - T.log(T.sum(T.exp(xdev), axis=1, keepdims=True))

# def categorical_crossentropy_logdomain(log_predictions, targets):
#     return -T.sum(targets * log_predictions, axis=1)

# In[17]:


# model = pickle.load(open('vgg_cnn_s.pkl'))
# CLASSES = model['synset words']
# MEAN_IMAGE = model['mean image']

X = T.tensor4()
y = T.matrix()
# vgg,driver = makeNeuralNet(X)
print "Constructing Neural Network"
driver = makeNeuralNet(X);
# print model['values']
# lasagne.layers.set_all_param_values(vgg, model['values'][:12])
# Load VGG CNN S into the vgg net.
def loadDriverModelFromFile(filename):
    print "Loading Neural Network Values from File"
    _v = np.load(filename)['model']
    lasagne.layers.set_all_param_values(driver, _v)
    if type(filename)==str:
        print "LOADED"
        return;
    args.modelStoreFile.close()
    print "Loaded!"


if args is not None and args.modelStoreFile is not None:
    loadDriverModelFromFile(args.modelStoreFile)


# training output
output_train = lasagne.layers.get_output(driver, deterministic=False)
# evaluation output. Also includes output of transform for plotting
output_eval = lasagne.layers.get_output(driver, deterministic=True)
loss = lasagne.objectives.squared_error(output_train, y).mean()

model_params = lasagne.layers.get_all_params(driver, trainable=True)

sh_lr = theano.shared(lasagne.utils.floatX(LEARNING_RATE))
# updates = lasagne.updates.adam(loss, model_params, learning_rate=sh_lr)
updates = lasagne.updates.momentum(loss, model_params, learning_rate=sh_lr)
print "Compiling Neural Network"

def detect_nan_in(i,node,fn):
    print "stuff";

def detect_nan_out(i, node, fn):
    for output in fn.outputs:
        if (not isinstance(output[0], np.random.RandomState) and
            np.isnan(output[0]).any()):
            print('*** NaN detected ***')
            print i, node
            theano.printing.debugprint(node)
            print('Inputs : %s' % [input[0] for input in fn.inputs])
            print('Outputs: %s' % [output[0] for output in fn.outputs])
            break

def inspect_inputs(i, node, fn):
    print i, node, "input(s) value(s):", [input[0] for input in fn.inputs]

def inspect_outputs(i, node, fn):
    print " output(s) value(s):", [output[0] for output in fn.outputs]

# mode = theano.compile.MonitorMode(pre_func=None, post_func=detect_nan_out).excluding('local_elemwise_fusion', 'inplace')
# mode = theano.compile.nanguardmode.NanGuardMode(nan_is_error=True, inf_is_error=True, big_is_error=True, optimizer='default', linker=None)
trainFn = theano.function([X, y], [loss, output_train], updates=updates, allow_input_downcast=True)
evalFn = theano.function([X], [output_eval])

numTrainFiles = 0
numValFiles = 0
numTestFiles = 0
if __name__== "__main__":
    import random, os
    def get_train_file():
        order = range(1, 30)
        random.shuffle(order)
        for folderID in order:
            fnames = os.listdir(DATA_DIR+str(folderID))
            fnames = sorted(fnames)
            for index,fn in enumerate(fnames):
                if index%8>5: #6,7 are reserved for val and things
                    continue
                yield DATA_DIR+str(folderID)+"/"+fn

    def get_val_file():
        order = range(1, 30)
        random.shuffle(order)
        for folderID in order:
            fnames = os.listdir(DATA_DIR+str(folderID))
            fnames = sorted(fnames)
            for index,fn in enumerate(fnames):
                if index%8==7: #6,7 are reserved for val and test
                    yield DATA_DIR+str(folderID)+"/"+fn

    def get_test_file():
        order = range(1, 30)
        random.shuffle(order)
        for folderID in order:
            fnames = os.listdir(DATA_DIR+str(folderID))
            fnames = sorted(fnames)
            for index,fn in enumerate(fnames):
                if index%8==6: #6,7 are reserved for val and test
                    yield DATA_DIR+str(folderID)+"/"+fn


    numTrainFiles = sum(1 for _ in get_train_file())
    numValFiles = sum(1 for _ in get_val_file())
    numTestFiles = sum(1 for _ in get_test_file())

def train_epoch():
    costs = []
    correct = 0
    total = 0
    bar = progressbar.ProgressBar(maxval=numTrainFiles, 
        widgets=['Train:', progressbar.Bar('=','[',']'),' ',progressbar.Percentage(),' ',progressbar.Counter(),'/',str(numTrainFiles),' ',progressbar.ETA(),' '])
    bar.start()
    count = 0;
    for file_name in get_train_file():
        f = open(file_name, 'r')
        memfile = StringIO.StringIO()
        memfile.write(lz4.loads(f.read()));
        f.close()
        memfile.seek(0)

        XY = np.load(memfile)
        X = XY["x"]
        y = XY["y"]
        cost_batch, output_train = trainFn(X, y)
        # print cost_batch, output_train
        if math.isnan(output_train[0][0]):
            raise Exception("NaN ERROR Caught %d"%i)
        costs += [cost_batch]
        correct += estCorrectness(output_train, y)
        total += y.shape[0]

        count += 1
        bar.update(count)

    bar.finish()

    return np.mean(costs), float(correct)/float(total)

def val_epoch():
    correct = 0.0;
    total = 0.0;
    bar = progressbar.ProgressBar(maxval=numValFiles, 
        widgets=['Val:', progressbar.Bar('=','[',']'),' ',progressbar.Percentage(),' ',progressbar.Counter(),'/',str(numValFiles),' ',progressbar.ETA(),' '])
    bar.start()
    count = 0
    for file_name in get_val_file():
        f = open(file_name, 'r')
        memfile = StringIO.StringIO()
        memfile.write(lz4.loads(f.read()));
        f.close()
        memfile.seek(0)

        XY = np.load(memfile)
        X = XY["x"]
        y = XY["y"]
        c,t = output_epoch(X,y)
        correct += c
        total += t
        count += 1
        bar.update(count)

    bar.finish()
    return correct / total


def test_epoch():
    correct = 0.0;
    total = 0.0;
    bar = progressbar.ProgressBar(maxval=numTestFiles, 
        widgets=['Test:', progressbar.Bar('=','[',']'),' ',progressbar.Percentage(),' ',progressbar.Counter(),'/',str(numTestFiles),' ',progressbar.ETA(),' '])
    bar.start()
    count = 0

    for file_name in get_test_file():
        f = open(file_name, 'r')
        memfile = StringIO.StringIO()
        memfile.write(lz4.loads(f.read()));
        f.close()
        memfile.seek(0)

        XY = np.load(memfile)
        X = XY["x"]
        y = XY["y"]
        c,t = output_epoch(X,y)
        correct += c
        total += t
        f.close()
        count += 1
        bar.update(count)

    bar.finish()

    return correct / total


def output_epoch(X,y):
    output_eval = evalFn(X)
    return float(estCorrectness(output_eval[0], y)), float(X.shape[0])

# > DATA INGESTION
#debug method.
def load_dataset(num):
    f = h5py.File("outTest/"+str(num)+".lz4", 'r')
    return f["x"], f["y"], f

if args is not None and args.repl:
    from ptpython.repl import embed
    embed(globals(), locals())


if __name__=="__main__":
    valid_accs, train_accs, test_accs = [], [], []
    try:
        for n in range(NUM_EPOCHS):
            print "Epoch:", n, " in progress..."
            train_cost, train_acc = train_epoch()
            valid_acc = val_epoch()
            test_acc = test_epoch()
            valid_accs += [valid_acc]
            test_accs += [test_acc]
            train_accs += [train_acc]

            if (n+1) % 20 == 0:
                new_lr = sh_lr.get_value() * 0.7
                print "New LR:", new_lr
                sh_lr.set_value(lasagne.utils.floatX(new_lr))

            print "Epoch {0}: Train cost {1}, Train acc {2}, val acc {3}, test acc {4}".format(
                    n, train_cost, train_acc, valid_acc, test_acc)
    except KeyboardInterrupt:
        pass

    print "Training complete. Saving..."
    #STORE THE MODEL
    import time
    vals = lasagne.layers.get_all_param_values(driver)
    modelFile = open("modelStore/"+time.strftime("%m%d-%H%M%S")+".model.pkl", mode="w")
    np.savez(modelFile, model=vals)
    print ">>>", modelFile.name
    modelFile.close()
    print "Done."


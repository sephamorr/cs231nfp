# from __future__ import print_function
# from nanomsg import Socket, PAIR, PUB
# s1 = Socket(PAIR)
# s2 = Socket(PAIR)
# s="ipc:///tmp/pipeline.ipc"
# s1.bind(s)
# s2.connect(s)
# s1.send(b'hello nanomsg')
# print(s1.recv())
# s1.close()
# s2.close()
import socket
import cv2
import numpy as np
import struct, time, sys, os
sys.path.append(os.path.abspath("arion/"))
import train as nn
import scipy.misc

nn.loadDriverModelFromFile("arion/modelStore/0306-185958.v2.live.model.pkl")

#create an INET, STREAMing socket
s = socket.socket(
    socket.AF_INET, socket.SOCK_STREAM)
#now connect to the web server on port 80
# - the normal http port
for i in range(100):
    try:
        s.connect(("localhost", 11111))
        break
    except:
        print ".",
        time.sleep(.2)
WIDTH=640
HEIGHT=480
DEPTH=3
BUFS=(WIDTH*HEIGHT*DEPTH)
# chunk=4096
# img=s.recv(BUFS)
key=10701000

keyRef = {
 0x80: 0, # 'shift',
 0x1000: 1,#'c',
 0xb00000:2,# 'left',
 0x500000:3, # 'right',
 0x5000000:4, # 'up',
 0xb000000:5,# 'down'}
}
keyRefRev = np.array([0x80,0x1000,0xb00000,0x500000,0x5000000,0xb000000], dtype=np.uint8)
paused = False
try:
    while True:
        toread=BUFS
        buf = bytearray(BUFS)
        view = memoryview(buf)
        while toread:
            nbytes = s.recv_into(view, toread)
            view = view[nbytes:] # slicing views is cheap
            toread -= nbytes
        imgstr=np.frombuffer(buf[::-1],dtype=np.uint8)
        # print imgstr.shape
        img=np.reshape(imgstr, (HEIGHT,WIDTH,DEPTH))[:,::-1,:]
        # print img.shape
        img = np.array(img, dtype=np.float32)/255.0
        img = scipy.misc.imresize(img[15:-15], (224,224), interp="nearest")
        img[20:45,:,:] = 0.0

        frame = img.swapaxes(0,1).swapaxes(0,2)

        key = 0x80;
        output =  nn.evalFn(np.array([frame], dtype=np.float32))[0]
        # for i in range(output.shape[1]):
        #     if(output[0,i]>0.5):
        #         key+=keyRefRev[i]
        r = np.random.random()
        t = 0.24
        if output[0]+r > 1.0+t: #X+E[0.5]>1.5
            key = key | 0x500000
        elif output[0]+r < 1.0-t: #X+E[0.5]<0.5
            key = key | 0xb00000
        
        if paused:
            key=0
            print "paused", '\r',
        else:
            print output, key
        s.send(struct.pack('I',key))
        # key+=1
        cv2.imshow("img",img)
        keyCode = cv2.waitKey(1)
        # if keyCode in [65361, 65363]:
        if keyCode == 65361:
            trainKey = [0.0,]
        elif keyCode == 65363:
            trainKey = [1.0,]
        elif keyCode == 65364:
            trainKey = [0.5,]
        elif keyCode == ord('q'):
            paused = not paused
            continue
        else:
            continue

        ##TRAIN ON THE TRAINKEY
        imgShown = frame.swapaxes(0,2).swapaxes(0,1)
        rotMat=cv2.getRotationMatrix2D((112,112), np.random.uniform(-3,3), 1.2)
        frame=cv2.warpAffine(imgShown, rotMat, (224,224)).swapaxes(0,1).swapaxes(0,2)

        print "TRAIN:", trainKey
        print "TROUT", nn.trainFn(np.array([frame,], dtype=np.float32), np.array([trainKey,], dtype=np.float32));
except KeyboardInterrupt:
    print "SAVING U"
    import time, lasagne
    vals = lasagne.layers.get_all_param_values(nn.driver)
    modelFile = open("arion/modelStore/"+time.strftime("%m%d-%H%M%S")+".v2.live.model.pkl", mode="w")
    np.savez(modelFile, model=vals)
    print ">>>", modelFile.name
    modelFile.close()
    print "Done."

# print len(buf)
# print type(buf)
# from __future__ import print_function
# from nanomsg import Socket, PAIR, PUB
# s1 = Socket(PAIR)
# s2 = Socket(PAIR)
# s="ipc:///tmp/pipeline.ipc"
# s1.bind(s)
# s2.connect(s)
# s1.send(b'hello nanomsg')
# print(s1.recv())
# s1.close()
# s2.close()
import socket
import cv2
import numpy as np
import struct, time, sys, os
# sys.path.append(os.path.abspath("arion/"))
# import train as nn
# import scipy.misc

# nn.loadDriverModelFromFile("arion/modelStore/0228-142949.model.pkl")

#create an INET, STREAMing socket
s = socket.socket(
    socket.AF_INET, socket.SOCK_STREAM)
#now connect to the web server on port 80
# - the normal http port
for i in range(100):
    try:
        s.connect(("localhost", 11111))
        break
    except:
        print ".",
        time.sleep(.2)
WIDTH=640
HEIGHT=480
DEPTH=3
BUFS=(WIDTH*HEIGHT*DEPTH)
# chunk=4096
# img=s.recv(BUFS)
key=10701000

keyRef = {
 0x80: 0, # 'shift',
 0x1000: 1,#'c',
 0xb00000:2,# 'left',
 0x500000:3, # 'right',
 0x5000000:4, # 'up',
 0xb000000:5,# 'down'}
}
keyRefRev = np.array([0x80,0x1000,0xb00000,0x500000,0x5000000,0xb000000], dtype=np.uint8)
skey=0;
k97=0.0
k100=0.0
while True:
    toread=BUFS
    buf = bytearray(BUFS)
    view = memoryview(buf)
    while toread:
        nbytes = s.recv_into(view, toread)
        view = view[nbytes:] # slicing views is cheap
        toread -= nbytes
    imgstr=np.frombuffer(buf[::-1],dtype=np.uint8)
    # print imgstr.shape
    img=np.reshape(imgstr, (HEIGHT,WIDTH,DEPTH))[:,::-1,:]
    # print img.shape
    # img = np.array(img, dtype=np.float32)/255.0
    # img = scipy.misc.imresize(img, (224,224), interp="nearest")
    # frame = img.swapaxes(0,1).swapaxes(0,2)

    key = 0L;
    # output =  nn.evalFn(np.array([frame], dtype=np.float32))[0]
    # for i in range(output.shape[1]):
    #     if(output[0,i]>0.5):
    #         key+=keyRefRev[i]
    cv2.imshow("img",img)
    k=cv2.waitKey(20)
    print k
    k97+=k==97
    k100+=k==100
    print "k97100",k97,k100
    if(k97>=1):
        k97=1.0
    if(k100>=1):
        k100=1.0
    if(k97>0):
        k97-=.2
        key+=0xb00000
    if(k100>0):
        k100-=.2
        key+=0x500000
    if(k97<0):
        k97=0.0
    if(k100<0):
        k100=0.0
    if(k==115):
        skey^=0x80;
        print "skey",skey
    key+=skey
    print key, '\r', 
    s.send(struct.pack('I',key))
    # key+=1
    
# print len(buf)
# print type(buf)

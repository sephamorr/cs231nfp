import socket
import cv2
import numpy as np
import struct, time, sys, os
sys.path.append(os.path.abspath("arion/"))
import train as nn
import scipy.misc
import theano
import theano.tensor as T
import lasagne
from collections import deque
import time, lasagne
import sys

import pygame


pygame.init()
screen = pygame.display.set_mode((660,730))
pygame.display.set_caption("Pod Driver Statistics")
myfont = pygame.font.SysFont("UbuntuMono", 15)
myfontsm = pygame.font.SysFont("UbuntuMono", 13)
FONT_AA = True

paused = True
slowmode = False

nnModelFile = "arion/modelStore/0307-002734.v2.live.model.pkl"
if len(sys.argv)==2:
    print "Will load from User specified model:", sys.argv[1]
    nnModelFile = sys.argv[1]

#       ______    ______   _______  
#      /      \  /      \ |       \ 
#     |  $$$$$$\|  $$$$$$\| $$$$$$$\
#     | $$  | $$| $$   \$$| $$__| $$
#     | $$  | $$| $$      | $$    $$
#     | $$  | $$| $$   __ | $$$$$$$\
#     | $$__/ $$| $$__/  \| $$  | $$
#      \$$    $$ \$$    $$| $$  | $$
#       \$$$$$$   \$$$$$$  \$$   \$$
#                                   

def createOCRNetwork(input_var=None):
    network = lasagne.layers.InputLayer(shape=(None, 1, 28, 28),
                                        input_var=input_var)
    network = lasagne.layers.Conv2DLayer(
            network, num_filters=32, filter_size=(5, 5),
            nonlinearity=lasagne.nonlinearities.rectify,
            W=lasagne.init.GlorotUniform())
    network = lasagne.layers.MaxPool2DLayer(network, pool_size=(2, 2))

    network = lasagne.layers.Conv2DLayer(
            network, num_filters=32, filter_size=(5, 5),
            nonlinearity=lasagne.nonlinearities.rectify)
    network = lasagne.layers.MaxPool2DLayer(network, pool_size=(2, 2))

    network = lasagne.layers.DenseLayer(
            lasagne.layers.dropout(network, p=.5),
            num_units=256,
            nonlinearity=lasagne.nonlinearities.rectify)

    network = lasagne.layers.DenseLayer(
            lasagne.layers.dropout(network, p=.5),
            num_units=10,
            nonlinearity=lasagne.nonlinearities.softmax)

    return network

kernel = np.ones((2,2),np.uint8)

p1=np.float32([[0,0],[0,1],[1,1]])
p2=np.float32([[-.1,0],[.1,1],[1.1,1]])
M=cv2.getAffineTransform(p1,p2)

input_var = T.tensor4('input_var')
ocrNet=createOCRNetwork(input_var=input_var)
# And load them again later on like this:
with np.load('speed/model.npz') as f:
    param_values = [f['arr_%d' % i] for i in range(len(f.files))]
lasagne.layers.set_all_param_values(ocrNet, param_values)

test_prediction = lasagne.layers.get_output(ocrNet, deterministic=True)
pred=T.argmax(test_prediction, axis=1)
ocrFn = theano.function([input_var], [pred])

def getSpeed(frame):
    #Slice out the images.
    framec=frame[360:420,450:550]
    l = np.array([240,180,0])
    u = np.array([255,220,20])

    #Image Processing on input.
    th=cv2.inRange(framec, l,u)
    th2=cv2.erode(th,kernel,iterations = 1)
    th3=cv2.dilate(th2,kernel,iterations = 1)

    thf=cv2.bitwise_and(framec,framec,mask=th3)
    rows,cols = th3.shape
    s = cv2.warpAffine(th3,M,(int(cols*1.3),rows))
    breaks=np.reshape(np.sum(s,axis=0),(1,-1))>5

    breaks=breaks
    going=False
    c=0
    imgs=[]
    for i in range(breaks.shape[1]):
        k=breaks[0,i]
        # print k
        if going and not k:
            rs=cv2.resize(s[:,c:i],(28,28))
            imgs.append(rs)
            going=False
        elif not going and k:
            c=i
            going=True
    dd=np.empty((len(imgs),1,28,28),dtype='float32')
    for i in range(len(imgs)):
        dd[i,0,:,:]=imgs[i]
    if len(imgs)==0:
        return 0
    ga=ocrFn(dd)[0]
    guess=0
    for gae in ga:
        guess=guess*10+gae
    return guess


#      _______             __                                
#     |       \           |  \                               
#     | $$$$$$$\  ______   \$$ __     __   ______    ______  
#     | $$  | $$ /      \ |  \|  \   /  \ /      \  /      \ 
#     | $$  | $$|  $$$$$$\| $$ \$$\ /  $$|  $$$$$$\|  $$$$$$\
#     | $$  | $$| $$   \$$| $$  \$$\  $$ | $$    $$| $$   \$$
#     | $$__/ $$| $$      | $$   \$$ $$  | $$$$$$$$| $$      
#     | $$    $$| $$      | $$    \$$$    \$$     \| $$      
#      \$$$$$$$  \$$       \$$     \$      \$$$$$$$ \$$      
#                                                            


nn.loadDriverModelFromFile(nnModelFile)

#create an INET, STREAMing socket
s = socket.socket(
    socket.AF_INET, socket.SOCK_STREAM)
#now connect to the web server on port 80
# - the normal http port
for i in range(100):
    try:
        s.connect(("localhost", 11111))
        break
    except:
        print ".",
        time.sleep(.2)

WIDTH=640
HEIGHT=480
DEPTH=3
BUFS=(WIDTH*HEIGHT*DEPTH)

# key=10701000

# keyRef = {
#  0x80: 0, # 'shift',
#  0x1000: 1,#'c',
#  0xb00000:2,# 'left',
#  0x500000:3, # 'right',
#  0x5000000:4, # 'up',
#  0xb000000:5,# 'down'}
# }

# keyRefRev = np.array([0x80,0x1000,0xb00000,0x500000,0x5000000,0xb000000], dtype=np.uint8)
history = deque()

def historyLength(currSpeed):
    return int((currSpeed/500.0)*29+2)

def getReward(historicalSpeed, historyDeque):
    #history is variable length.
    if historicalSpeed < 120:
        return 0;
    speed_history= [historicalSpeed,]
    speed_history += [x[2] for x in historyDeque]
    shis = np.array(speed_history, copy=True, dtype=np.float32)
    shis_diff = np.diff(shis)
    diff_weight = np.linspace(5, 1, num=shis_diff.shape[0]) / (6.0*shis.shape[0]/2.0)
    s_a = np.sum(shis_diff * diff_weight)
    s_a = np.clip(s_a, -5, 1)
    if historicalSpeed > 340 and speed_history[-1] > 340:
        s_a += 0.5
    if speed_history[-1]<260:
        s_a -= 2.0
    s_a = s_a / 50.0
    return s_a

salFn = nn.compile_saliency(nn.net)

def drawBar(val,minVal,maxVal,posX,posY,length,height,baseColor=(30,30,30),topColor=(255,255,255)):
    pygame.draw.rect(screen, baseColor, (posX,posY,length,height))
    newX = float(val-minVal)/(maxVal-minVal)*(length-height) + posX
    pygame.draw.rect(screen, topColor, (newX, posY, height, height))

capturingKeys = set([pygame.K_UP, pygame.K_RIGHT, pygame.K_LEFT, pygame.K_p, pygame.K_t, pygame.K_s])
clock = pygame.time.Clock()
try:
    while True:
        ##Display##
        pygame.display.flip()
        
        if slowmode:
            clock.tick(5)

        keyCode = None
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                raise KeyboardInterrupt
            elif event.type == pygame.KEYDOWN:
                if event.key in capturingKeys:
                    keyCode = event.key;

        screen.fill((0,0,0))

        ##-----IO-----##
        toread=BUFS
        buf = bytearray(BUFS)
        view = memoryview(buf)
        while toread:
            nbytes = s.recv_into(view, toread)
            view = view[nbytes:] # slicing views is cheap
            toread -= nbytes

        imgstr=np.frombuffer(buf[::-1],dtype=np.uint8)
        img=np.reshape(imgstr, (HEIGHT,WIDTH,DEPTH))[:,::-1,:]
        img_noscale=np.array(img, dtype=np.float32)
        this_speed = getSpeed(img_noscale)
        img = img_noscale/255.0
        img = scipy.misc.imresize(img[15:-15], (224,224), interp="nearest")
        img[16:40,:,:] = 0.0

        frame = img.swapaxes(0,1).swapaxes(0,2)

        key = 0x80;
        saliency,evaluation = salFn(np.array([frame,], dtype=np.float32))
        sal = (saliency[0])[::-1].transpose(1,2,0)

        # cv2.namedWindow("pos=saliency", cv2.WINDOW_NORMAL)
        # cv2.namedWindow("abs=saliency", cv2.WINDOW_NORMAL)

        # cv2.imshow("pos=saliency",np.maximum(0, sal)/sal.max())
        # cv2.imshow("abs=saliency",np.abs(sal)/(np.abs(sal).max()))

        t = 0.08
        r = np.random.normal(0.0, t)
        output = None
        if evaluation[0]+r > 1.9/3.0: #X+E[0.5]>1.5
            key = key | 0x500000
            output = [[1.0, ]]
        elif evaluation[0]+r < 1.1/3.0: #X+E[0.5]<0.5
            key = key | 0xb00000
            output = [[0.0, ]]
        else:
            output = [[0.5,]]
        if paused:
            key=0
            label = myfont.render("Output & Action (Paused!)", FONT_AA, (255,100,100))
            screen.blit(label, (30, 10))
        else:
            #Printing the evaluation:
            label = myfont.render("Output & Action", FONT_AA, (255,255,255))
            screen.blit(label, (30, 10))
            drawBar(evaluation[0][0], 0, 1, 30, 275, 224, 10)
            drawBar(output[0][0],     0, 1, 30, 290, 224, 10)

        s.send(struct.pack('I',key))

        if not paused:
            history.append((frame, output, this_speed))


        img_sfc = pygame.surfarray.make_surface(img.swapaxes(0,1)[:,:,[2,1,0]])
        screen.blit(img_sfc, (30,50))

        # sal_abs = (np.abs(sal)/(np.abs(sal).max()))*255.0
        sal_abs = (np.minimum(0,sal) / sal.min()) * 255.0
        img_sal_abs = pygame.surfarray.make_surface(sal_abs.swapaxes(0,1)[:,:,[2,1,0]])
        screen.blit(img_sal_abs, (30,480))

        sal_abs = (np.maximum(0, sal)/sal.max())*255.0
        img_sal_abs = pygame.surfarray.make_surface(sal_abs.swapaxes(0,1)[:,:,[2,1,0]])
        screen.blit(img_sal_abs, (260,480))

        label = myfont.render("Positive Saliency",  FONT_AA, (255,255,255))
        screen.blit(label, (260,480))

        label = myfont.render("Negative Saliency",  FONT_AA, (255,255,255))
        screen.blit(label, (30,480))

        label = myfontsm.render("[p] to pause",  FONT_AA, (255,255,255))
        screen.blit(label, (260, 380))
        label = myfontsm.render("[t] to enter REPL",  FONT_AA, (255,255,255))
        screen.blit(label, (260, 400))
        label = myfontsm.render("[left/right/up] to train this frame", FONT_AA, (255,255,255))
        screen.blit(label, (260, 420))
        label = myfontsm.render("[s] to enter slow mode",  FONT_AA, (255,255,255))
        screen.blit(label, (260, 440))


        ##draw history:
        label = myfont.render("Reinforcement Learning", FONT_AA, (255,255,255))
        screen.blit(label, (260, 10))
        history_length = historyLength(this_speed)
        history_height = len(history)*10
        h_c = 0;
        for i_h,h_h in reversed(list(enumerate([x[1:] for x in history]))):
            fc = (130,130,130)
            if h_c > history_length:
                break;
            drawBar(h_h[0][0][0], 0, 1, 260, 30+history_height - h_c*10, 224, 10, topColor=(190,190,190))
            drawBar(h_h[1], 0, 400, 260+226, 30+history_height - h_c*10, 130, 4, topColor=(190,190,190))
            h_c += 1


        #REINFORCEMENT LEARNING
        while len(history) > history_length and not paused:
            his_frame,his_output,his_speed = history.popleft()
            h_c = len(history)
            reward = getReward(his_speed, history)
            #Graphics Output:
            if reward > 0:
                fc = (120,255,120)
            elif reward<0:
                fc = (255,15,15)
            elif reward > -1.0/50:
                fc = (80,80,80)

            # h_c += 1
            drawBar(his_output[0][0], 0, 1, 260, 30+history_height - h_c*10, 224, 10, topColor=fc)
            drawBar(his_speed, 0, 400, 260+226, 30+history_height - h_c*10, 130, 4, topColor=fc)
            #Reinforcement Learning Logic
            if(reward > 0):
                # print reward, "+"*int(reward*50)
                pass
            elif(reward < 0):
                his_output = np.array(his_output)
                his_output = 1.0-his_output;
                reward = -reward
                # print reward, "-"*int(-reward*50)
            elif reward > -1.0/50 or this_speed == 0: #catches 0~very tiny negative.
                continue;

            drawBar(abs(reward), 0, 1.0, 260+226, 35+history_height - h_c*10, 130, 4, topColor=(100,100,255))

            newLearningRate = reward * 0.00004
            if his_output[0][0] == 0.5:
                newLearningRate /= 100.0
            
            imgShown = his_frame.swapaxes(0,2).swapaxes(0,1)
            rotMat=cv2.getRotationMatrix2D((112,112), np.random.uniform(-3,3), 1.2)
            his_frame=cv2.warpAffine(imgShown, rotMat, (224,224)).swapaxes(0,1).swapaxes(0,2)

            nn.sh_lr.set_value(np.float32(newLearningRate))
            # print "train", nn.trainFn(np.array([his_frame, ], dtype=np.float32), his_output)


        # keyCode = cv2.waitKey(1)
        trainKey = None
        if keyCode == pygame.K_LEFT:
            trainKey = [0.0,]
        elif keyCode == pygame.K_RIGHT:
            trainKey = [1.0,]
        elif keyCode == pygame.K_UP:
            trainKey = [0.5,]
        elif keyCode == pygame.K_p:
            paused = not paused
            continue
        elif keyCode == pygame.K_s:
            slowmode = not slowmode
            continue
        elif keyCode == pygame.K_t:
            from ptpython.repl import embed
            embed(globals(), locals())
            continue
        elif paused:
            continue
        else:
            continue

        ##TRAIN ON THE TRAINKEY
        nn.sh_lr.set_value(np.float32(0.0008))
        imgShown = frame.swapaxes(0,2).swapaxes(0,1)
        rotMat=cv2.getRotationMatrix2D((112,112), np.random.uniform(-3,3), 1.2)
        frame=cv2.warpAffine(imgShown, rotMat, (224,224)).swapaxes(0,1).swapaxes(0,2)

        print "TRAIN:", trainKey
        print "TROUT", nn.trainFn(np.array([frame,], dtype=np.float32), np.array([trainKey,], dtype=np.float32));
except KeyboardInterrupt:
    print "SAVING U"
    vals = lasagne.layers.get_all_param_values(nn.driver)
    modelFile = open("arion/modelStore/"+time.strftime("%m%d-%H%M%S")+".v2.live.model.pkl", mode="w")
    np.savez(modelFile, model=vals)
    print ">>>", modelFile.name
    modelFile.close()
    print "Done."
